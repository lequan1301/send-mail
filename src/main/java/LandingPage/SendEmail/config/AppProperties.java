package LandingPage.SendEmail.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("app")
public class AppProperties {

    private Integer defaultPageIndex;
    private Integer defaultPageSize;
    private String fullPath;
    private Short eventId;
    private String downloadPath;

}
