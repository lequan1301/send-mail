package LandingPage.SendEmail.constant;

public class UploadType {
  public final static int  TYPE_UPLOAD_EVENT = 1;
  public final static int  TYPE_UPLOAD_LIST_DELEGATION = 6;
  public final static int  TYPE_UPLOAD_IMAGE_DELEGATION = 8;
  public final static int  TYPE_UPLOAD_DELEGATION_UNION = 7;
  public final static int  TYPE_UPLOAD_LOGO_VOTE = 2;
  public final static int  TYPE_UPLOAD_CONTENT_VOTE = 3;
  public final static int  TYPE_UPLOAD_DOCUMENT_SESSION = 4;
  public final static int  TYPE_UPLOAD_LOGO_SESSION= 5;
  public final static int  TYPE_UPLOAD_LIST_DELEGATION_UNION = 9;

  public final static int  SIZE_1MB = 1048576;
  public final static int  SIZE_5MB = 5242880;
  public final static int  SIZE_10MB = 10485760;

}
