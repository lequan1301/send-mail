package LandingPage.SendEmail.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UploadFileResponse {
    private String path;
    private String fullPath;
    private Long docSize;
    private String extension;
}
