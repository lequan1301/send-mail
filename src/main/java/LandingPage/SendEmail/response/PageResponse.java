package LandingPage.SendEmail.response;

import lombok.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PageResponse {
    private int current;
    private int size;
    private int total_pages;
    private int total_records;
}
