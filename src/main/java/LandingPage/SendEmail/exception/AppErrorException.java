package LandingPage.SendEmail.exception;

import LandingPage.SendEmail.constant.CommonMessage;
import io.jsonwebtoken.lang.Strings;
import lombok.Getter;
import org.springframework.http.HttpStatus;

public class AppErrorException extends Exception {

    private CommonMessage cmMsg;

    @Getter
    private Integer code;

    @Getter
    private String message;

    @Getter
    private HttpStatus statusCode;

    public AppErrorException() {
        this.code = CommonMessage.FAILED.code;
        this.message = CommonMessage.FAILED.message;
        this.statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    public AppErrorException(CommonMessage cmMsg, HttpStatus statusCode) {
        this.code = cmMsg.code;
        this.message = cmMsg.message;
        this.statusCode = statusCode;
    }

    public AppErrorException(CommonMessage cmMsg, String replaceStr) {
        this.code = cmMsg.code;
        this.message = Strings.replace(cmMsg.message, "{}", replaceStr);
    }

}
