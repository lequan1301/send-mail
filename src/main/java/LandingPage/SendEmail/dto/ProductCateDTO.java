package LandingPage.SendEmail.dto;

import LandingPage.SendEmail.entity.Category;
import LandingPage.SendEmail.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductCateDTO {
   List<Product> products;
   Category category;
}
