package LandingPage.SendEmail.dto;

import LandingPage.SendEmail.entity.Category;
import LandingPage.SendEmail.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    String username;
    String password;
}
