package LandingPage.SendEmail.request;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
public class ProductRequest {
    private String keyword;
    private int pageIndex;
    private int pageSize;
}
