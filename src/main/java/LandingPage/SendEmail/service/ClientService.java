package LandingPage.SendEmail.service;

import LandingPage.SendEmail.service.sdi.ClientSdi;

public interface ClientService {
    Boolean create(ClientSdi sdi);
}