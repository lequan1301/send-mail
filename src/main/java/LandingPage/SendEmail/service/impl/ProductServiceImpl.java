package LandingPage.SendEmail.service.impl;

import LandingPage.SendEmail.constant.CommonMessage;
import LandingPage.SendEmail.controller.response.CategoryResponse;
import LandingPage.SendEmail.dto.ProductCateDTO;
import LandingPage.SendEmail.entity.Category;
import LandingPage.SendEmail.entity.Product;
import LandingPage.SendEmail.repository.CategoryRepository;
import LandingPage.SendEmail.repository.ProductRepository;
import LandingPage.SendEmail.request.ProductRequest;
import LandingPage.SendEmail.response.BaseResponse;
import LandingPage.SendEmail.response.PageResponse;
import LandingPage.SendEmail.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;
    private CategoryRepository categoryRepository;

    @Override
    public Product createProduct(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product getProductById(Long productId) {
        Optional<Product> optionalProduct = productRepository.findById(productId);
        return optionalProduct.get();
    }

    @Override
    public List<Product> getAllProduct() {
        return productRepository.findAll();
    }

    @Override
    public BaseResponse<?> findAllProductPage(ProductRequest productRequest) {
        int pageIndex = productRequest.getPageIndex() == 0 ? 1 : productRequest.getPageIndex();
        int pageSize = productRequest.getPageSize() == 0 ? 20 : productRequest.getPageSize();
        Pageable pageable = PageRequest.of(pageIndex - 1, pageSize);
        Page<Product> productDaoPage = productRepository.findAll(pageable);
        Map<String, Object> map = new HashMap<>();
        List<Product> products = new ArrayList<>();
        for (Product product : productDaoPage.getContent()) {
            products.add(product);
        }
        PageResponse pageResponse = new PageResponse(pageIndex, pageSize, productDaoPage.getTotalPages(), (int) productDaoPage.getTotalElements());
        map.put("items", products);
        map.put("paging", pageResponse);
        return new BaseResponse<>(CommonMessage.SUCCESS, map);
    }

    @Override
    public List<ProductCateDTO> getProductByCate() {
        List<ProductCateDTO> response = new ArrayList<>();
        List<Category> categories = categoryRepository.findAll();
        for (Category category : categories) {
            List<Product> products = new ArrayList<>();
            products.addAll(productRepository.findByCate(category.getId()));
            response.add(new ProductCateDTO(){{
                setCategory(category);
                setProducts(products);
            }});
        }
        return response;
    }

    @Override
    public Product updateProduct(Product product) {
        Product existingProduct = productRepository.findById(product.getId()).get();
        existingProduct.setCatId(product.getCatId());
        existingProduct.setName(product.getName());
        existingProduct.setOrigin(product.getOrigin());
        existingProduct.setPrice(product.getPrice());
        existingProduct.setImage(product.getImage());
        existingProduct.setDescription(product.getDescription());
        existingProduct.setAmount(product.getAmount());
        Product updateProduct = productRepository.save(existingProduct);
        return updateProduct;
    }

    @Override
    public void deleteProduct(Long productId) {
        productRepository.deleteById(productId);
    }
}
