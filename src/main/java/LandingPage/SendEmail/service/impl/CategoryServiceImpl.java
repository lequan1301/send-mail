package LandingPage.SendEmail.service.impl;

import LandingPage.SendEmail.controller.response.CategoryResponse;
import LandingPage.SendEmail.entity.Category;
import LandingPage.SendEmail.repository.CategoryRepository;
import LandingPage.SendEmail.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    @Value("${spring.datasource.url}")
    private String url;

    private final CategoryRepository categoryRepository;

    @Override
    public Category createCategory(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public Category getCategoryById(Long categoryId) {
        Optional<Category> optionalCategory = categoryRepository.findById(categoryId);
        return optionalCategory.get();
    }

    @Override
    public CategoryResponse getAllCategory(){
        List<Category> categories = categoryRepository.findAll();
        String additionalValue = "Giá trị bổ sung của bạn";
        return new CategoryResponse(categories, url);
    }

    @Override
    public Category updateCategory(Category category) {
        Category existingCategory = categoryRepository.findById(category.getId()).get();
        existingCategory.setName(category.getName());
        existingCategory.setDescription(category.getDescription());
        Category updateCategory = categoryRepository.save(existingCategory);
        return updateCategory;
    }

    @Override
    public void deleteCategory(Long categoryId) {
        categoryRepository.deleteById(categoryId);
    }
}
