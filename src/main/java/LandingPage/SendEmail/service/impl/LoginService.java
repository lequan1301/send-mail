package LandingPage.SendEmail.service.impl;

import LandingPage.SendEmail.dto.UserDTO;
import LandingPage.SendEmail.entity.User;
import LandingPage.SendEmail.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LoginService {
    private UserRepository userRepository;

    public LoginService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Optional<Integer> login(UserDTO user) {
        User newUser = userRepository.findByUserName(user.getUsername());
        System.out.println(newUser);
        System.out.println(user.getUsername());
        if (newUser != null) {
            String password = newUser.getPassword();
            if (password != null && password.equals(user.getPassword())) {
                return Optional.of(1);
            }
        }

        return Optional.empty();
    }
}
