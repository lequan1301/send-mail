package LandingPage.SendEmail.service.impl;

import LandingPage.SendEmail.config.AppProperties;
import LandingPage.SendEmail.constant.CommonMessage;
import LandingPage.SendEmail.constant.UploadType;
import LandingPage.SendEmail.response.BaseResponse;
import LandingPage.SendEmail.response.UploadFileResponse;
import jakarta.servlet.ServletContext;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.MimetypesFileTypeMap;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

@Service
@Slf4j
@RequiredArgsConstructor
public class StorageService {

    @Value("${upload.path}")
    String pathUpload;

    private final ServletContext context;

    private final AppProperties appProperties;

    /*
    * upload file type:
    * 1: logo dai hoi
    * 2: anh minh họa biểu quyết
    * 3: ảnh câu hỏi biểu quyết
    * 4: tài liệu phiên dh
    * 5: sơ đồ phiên
    * 6: danh sach upload đại biểu
    * 7: đoàn đại biểu
    * 8: avatar đại biểu
    */
    public BaseResponse store(MultipartFile file, Integer type) {
        UploadFileResponse uploadFileResponse = new UploadFileResponse();
        log.info("file.getOriginalFilename() {}", new MimetypesFileTypeMap().getContentType(file.getOriginalFilename()));
        String mimetype = new MimetypesFileTypeMap().getContentType(file.getOriginalFilename());
        String typeF = FilenameUtils.getExtension(file.getOriginalFilename());

        log.info("typeF {}", typeF);
        if (file.isEmpty()) {
            return new BaseResponse(CommonMessage.FILE_NOT_EMPTY_DEFAULT);
        }
        switch (type) {
            case UploadType.TYPE_UPLOAD_EVENT, UploadType.TYPE_UPLOAD_LOGO_SESSION -> {
                if (file.getSize() > UploadType.SIZE_1MB) {
                    return new BaseResponse(CommonMessage.FILE_TOO_LARGE_EVENT_DEFAULT);
                }
                if (!mimetype.contains("image"))
                    return new BaseResponse(CommonMessage.FILE_INVALID_EVENT_DEFAULT);
                break;
            }

            case UploadType.TYPE_UPLOAD_LIST_DELEGATION -> {
                if (file.getSize() > UploadType.SIZE_5MB) {
                    return new BaseResponse(CommonMessage.FILE_TOO_LARGE_DELEGATION);
                }

                if (typeF == null || !(typeF.equalsIgnoreCase("xlsx")))
                    return new BaseResponse(CommonMessage.FILE_INVALID_EVENT_DEFAULT);

                // check content file
                // return
                break;
            }
            case UploadType.TYPE_UPLOAD_IMAGE_DELEGATION -> {
                if (file.getSize() > UploadType.SIZE_1MB) {
                    return new BaseResponse(CommonMessage.FILE_TOO_LARGE_DELEGATION);
                }

                if (typeF == null ||
                        !((typeF.equalsIgnoreCase("img")) ||
                                (typeF.equalsIgnoreCase("jpg")) ||
                                (typeF.equalsIgnoreCase("png")) ||
                                (typeF.equalsIgnoreCase("jpeg"))
                ))
                    return new BaseResponse(CommonMessage.FILE_INVALID_EVENT_DEFAULT);

                break;
            }

            case UploadType.TYPE_UPLOAD_CONTENT_VOTE -> {
                if (file.getSize() > UploadType.SIZE_10MB) {
                    return new BaseResponse(CommonMessage.FILE_TOO_LARGE_IMAGE_VOTE);
                }
                if (!mimetype.contains("image"))
                    return new BaseResponse(CommonMessage.FILE_INVALID_IMAGE_VOTE);
                break;
            }
            case UploadType.TYPE_UPLOAD_DOCUMENT_SESSION -> {
                break;
            }

            // đoàn đại biểu
            case UploadType.TYPE_UPLOAD_DELEGATION_UNION -> {
                if (file.getSize() > UploadType.SIZE_5MB) return new BaseResponse(CommonMessage.FILE_TOO_LARGE_DELEGATION);
                if (typeF == null || (!typeF.contains("xlsx") && !typeF.contains("xls") && !typeF.contains("csv"))) return new BaseResponse(CommonMessage.FILE_INVALID_EVENT_DEFAULT);

                // import doan dai bieu
                break;
            }
            default -> {
                if (file.getSize() > UploadType.SIZE_1MB) {
                    return new BaseResponse(CommonMessage.FILE_TOO_LARGE_EVENT_DEFAULT);
                }
                if (!mimetype.contains("image"))
                    return new BaseResponse(CommonMessage.FILE_INVALID_EVENT_DEFAULT);
                break;
            }

        }

        try {
            log.info("pathUpload : {}", pathUpload);
            String pathReal = pathUpload + type + "/";
            Path directory = Paths.get(pathReal);
            log.info("toAbsolutePath : {}", directory.toAbsolutePath());
            if (!Files.exists(directory)) {
                log.info("directory not exists");
                Files.createDirectories(directory.toAbsolutePath());

                // If you require it to make the entire directory path including parents,
                // use directory.mkdirs(); here instead.
            }
            // Get the file and save it uploads dir
            log.info("directory exists");
            //size MB
            //Double fileSize = (double) Math.round((float) ((file.getSize() / (1024 * 1024)) * 10) /10);
            byte[] bytes = file.getBytes();
            String convertName = StringEscapeUtils.unescapeHtml4(file.getOriginalFilename()).replaceAll(Pattern.compile("[{}()\\[\\] +*?^$\\\\|]").pattern(), "_");

            String pattern = "ddMMyyyy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            convertName = simpleDateFormat.format(new Date()) + "_" + convertName;
            Path path = Paths.get(pathReal, convertName);
            Files.write(path, bytes);
            uploadFileResponse.setPath("/event/file/" + type + "/" + convertName);
            uploadFileResponse.setFullPath(appProperties.getFullPath() + "event/file/" + type + "/" + convertName);
            uploadFileResponse.setDocSize(file.getSize());
            uploadFileResponse.setExtension(typeF);
            return new BaseResponse(CommonMessage.SUCCESS, uploadFileResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new BaseResponse(CommonMessage.FAILED);
    }

    public ResponseEntity view(String fileName, String type) throws FileNotFoundException {

        String pathX = pathUpload + type + "/" + fileName;
        File file2Upload = new File(pathX);

        String mimetype = new MimetypesFileTypeMap().getContentType(file2Upload.getName());
        Integer typef = mimetype.contains("image") ? 1 : 0;

        if (file2Upload != null) {
            HttpHeaders headers = new HttpHeaders();
            if (typef == 0) {
                headers.add("Content-Disposition", "attachment; filename=" + fileName);
            }
            InputStreamResource inputStream = new InputStreamResource(new FileInputStream(file2Upload));
            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .contentType(typef == 1 ? MediaType.IMAGE_JPEG :  MediaType.parseMediaType("application/octet-stream"))
                    .body(inputStream);
        } else {
            return new ResponseEntity<>(CommonMessage.FAILED, HttpStatus.OK);
        }
    }
    public ResponseEntity download(String fileName, String type) throws FileNotFoundException {

        String pathX = pathUpload + type + "/" + fileName;
        File file2Upload = new File(pathX);

        String mimetype = new MimetypesFileTypeMap().getContentType(file2Upload.getName());
        Integer typef = mimetype.contains("image") ? 1 : 0;

        if (file2Upload.exists()) {
            HttpHeaders headers = new HttpHeaders();
           // if (typef == 0) {
                headers.add("Content-Disposition", "attachment; filename=" + fileName);
          //  }
            InputStreamResource inputStream = new InputStreamResource(new FileInputStream(file2Upload));
            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .contentType(typef == 1 ? MediaType.IMAGE_JPEG : MediaType.parseMediaType("application/octet-stream"))
                    .body(inputStream);
        } else {
            return new ResponseEntity<>(CommonMessage.FAILED, HttpStatus.OK);
        }
    }
}
