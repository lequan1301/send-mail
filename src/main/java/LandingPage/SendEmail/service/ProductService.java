package LandingPage.SendEmail.service;

import LandingPage.SendEmail.dto.ProductCateDTO;
import LandingPage.SendEmail.entity.Product;
import LandingPage.SendEmail.request.ProductRequest;
import LandingPage.SendEmail.response.BaseResponse;
import org.springframework.data.domain.Page;

import java.awt.print.Pageable;
import java.util.List;

public interface ProductService {
    Product createProduct(Product product);

    Product getProductById(Long productId);

    List<Product> getAllProduct();

    BaseResponse<?> findAllProductPage(ProductRequest productRequest);

    Product updateProduct(Product product);

    void deleteProduct(Long productId);

    List<ProductCateDTO> getProductByCate();
}
