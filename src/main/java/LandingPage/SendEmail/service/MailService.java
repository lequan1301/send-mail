package LandingPage.SendEmail.service;

import LandingPage.SendEmail.dto.DataMailDTO;
import org.springframework.messaging.MessagingException;

public interface MailService {
    void sendHtmlMail(DataMailDTO dataMail, String templateName) throws MessagingException;
}