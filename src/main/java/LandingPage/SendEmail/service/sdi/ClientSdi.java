package LandingPage.SendEmail.service.sdi;

import lombok.Data;

@Data
public class ClientSdi {
    private String name;
    private String username;
    private String email;
}
