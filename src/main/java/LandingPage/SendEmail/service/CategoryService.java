package LandingPage.SendEmail.service;

import LandingPage.SendEmail.controller.response.CategoryResponse;
import LandingPage.SendEmail.entity.Category;

import java.util.List;

public interface CategoryService {
    Category createCategory(Category category);

    Category getCategoryById(Long categoryId);

    CategoryResponse getAllCategory();

    Category updateCategory(Category category);

    void deleteCategory(Long categoryId);
}
