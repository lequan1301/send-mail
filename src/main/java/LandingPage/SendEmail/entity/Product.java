package LandingPage.SendEmail.entity;

import jakarta.persistence.*;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private Long catId;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String origin;
    @Column(nullable = false)
    private Double price;
    @Column(nullable = false)
    private String image;
    @Column(nullable = false)
    private String description;
    @Column(nullable = false)
    private Integer amount;
}
