package LandingPage.SendEmail.controller;

import LandingPage.SendEmail.controller.response.CategoryResponse;
import LandingPage.SendEmail.entity.Category;
import LandingPage.SendEmail.service.CategoryService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("api/category")
@CrossOrigin("*")
public class CategoryController {

    private CategoryService categoryService;

    // build create Category REST API
    @PostMapping
    public ResponseEntity<Category> createCategory(@RequestBody Category category){
        Category savedCategory = categoryService.createCategory(category);
        return new ResponseEntity<>(savedCategory, HttpStatus.CREATED);
    }

    // build get user by id REST API
    // http://localhost:8080/api/category/1
    @GetMapping("{id}")
    public ResponseEntity<Category> getCategoryById(@PathVariable("id") Long categoryId){
        Category category = categoryService.getCategoryById(categoryId);
        return new ResponseEntity<>(category, HttpStatus.OK);
    }

    // Build Get All Categorys REST API
    // http://localhost:8080/api/category
    @GetMapping
    public ResponseEntity<CategoryResponse> getAllCategory(){
        CategoryResponse category = categoryService.getAllCategory();
        return new ResponseEntity<>(category, HttpStatus.OK);
    }

    // Build Update Category REST API
    @PutMapping("{id}")
    // http://localhost:8080/api/category/1
    public ResponseEntity<Category> updateCategory(@PathVariable("id") Long categoryId,
                                           @RequestBody Category category){
        category.setId(categoryId);
        Category updatedCategory = categoryService.updateCategory(category);
        return new ResponseEntity<>(updatedCategory, HttpStatus.OK);
    }

    // Build Delete Category REST API
    @DeleteMapping("{id}")
    public ResponseEntity<String> deleteCategory(@PathVariable("id") Long categoryId){
        categoryService.deleteCategory(categoryId);
        return new ResponseEntity<>("Category successfully deleted!", HttpStatus.OK);
    }
}
