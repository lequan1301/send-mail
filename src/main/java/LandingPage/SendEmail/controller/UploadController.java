package LandingPage.SendEmail.controller;

import LandingPage.SendEmail.response.BaseResponse;
import LandingPage.SendEmail.response.UploadFileResponse;
import LandingPage.SendEmail.service.impl.StorageService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api")
@Slf4j
@CrossOrigin
@AllArgsConstructor
public class UploadController {

    private final StorageService storageService;

    @PostMapping(path = "/upload",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public BaseResponse<UploadFileResponse> handleFileUpload(
            @RequestParam(value = "file") MultipartFile file, @RequestParam(value = "file_name", required = false, defaultValue = "") String fileName,
            @RequestParam(value = "type") Integer type) {
        log.info("uploads");
        return storageService.store(file, type);
    }

    @GetMapping("/event/file/{type}/{file_name}")
    public ResponseEntity<?> viewFile(@PathVariable(name = "file_name") String fileName,@PathVariable(name = "type") String type) throws IOException {
        log.info("download");
        return storageService.view(fileName,type);
    }
    @GetMapping("/event/file/download/{type}/{file_name}")
    public ResponseEntity<?> downloadFile(@PathVariable(name = "file_name") String fileName,@PathVariable(name = "type") String type) throws IOException {
        log.info("download");
        return storageService.download(fileName,type);
    }
}