package LandingPage.SendEmail.controller.response;

import LandingPage.SendEmail.entity.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryResponse {
    List<Category> categories;
    String additionalValue;
}
