package LandingPage.SendEmail.controller;

import LandingPage.SendEmail.dto.ProductCateDTO;
import LandingPage.SendEmail.entity.Product;
import LandingPage.SendEmail.request.ProductRequest;
import LandingPage.SendEmail.response.BaseResponse;
import LandingPage.SendEmail.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("api/product")
@CrossOrigin("*")
public class ProductController {

    private ProductService productService;

    // build create Product REST API
    @PostMapping
    public ResponseEntity<Product> createProduct(@RequestBody Product product){
        Product savedProduct = productService.createProduct(product);
        return new ResponseEntity<>(savedProduct, HttpStatus.CREATED);
    }

    // build get user by id REST API
    // http://localhost:8080/api/product/1
    @GetMapping("{id}")
    public ResponseEntity<Product> getProductById(@PathVariable("id") Long productId){
        Product product = productService.getProductById(productId);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    // Build Get All Products REST API
    // http://localhost:8080/api/product
    @GetMapping
    public ResponseEntity<List<Product>> getAllProduct(){
        List<Product> product = productService.getAllProduct();
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @GetMapping("/pagination")
    public BaseResponse<?> findAllByName(@RequestBody ProductRequest productRequest){
        return productService.findAllProductPage(productRequest);
    }

    @GetMapping("cate")
    public ResponseEntity<List<ProductCateDTO>> getAllProductByCate(){
        List<ProductCateDTO> productByCate = productService.getProductByCate();
        return new ResponseEntity<>(productByCate, HttpStatus.OK);
    }

    // Build Update Product REST API
    @PutMapping("{id}")
    // http://localhost:8080/api/product/1
    public ResponseEntity<Product> updateProduct(@PathVariable("id") Long productId,
                                                   @RequestBody Product product){
        product.setId(productId);
        Product updatedProduct = productService.updateProduct(product);
        return new ResponseEntity<>(updatedProduct, HttpStatus.OK);
    }

    // Build Delete Product REST API
    @DeleteMapping("{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable("id") Long productId){
        productService.deleteProduct(productId);
        return new ResponseEntity<>("Product successfully deleted!", HttpStatus.OK);
    }
}
