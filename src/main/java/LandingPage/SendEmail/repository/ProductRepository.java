package LandingPage.SendEmail.repository;

import LandingPage.SendEmail.entity.Product;
import LandingPage.SendEmail.request.ProductRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Long> {

    Product save(Product product);

    Optional<Product> findById(Long productId);

    void deleteById(Long productId);

    List<Product> findAll();

    @Query(value="select * from product", nativeQuery=true)
    Page<Product> findAll(Pageable pageable);

    @Query(value = "select p from Product p where p.catId = :catId")
    List<Product> findByCate(Long catId);

}