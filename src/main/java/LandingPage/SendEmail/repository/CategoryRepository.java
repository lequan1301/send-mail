package LandingPage.SendEmail.repository;

import LandingPage.SendEmail.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    Category save(Category category);

    Optional<Category> findById(Long categoryId);

    void deleteById(Long categoryId);

    List<Category> findAll();
}
