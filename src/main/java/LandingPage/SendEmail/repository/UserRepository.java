package LandingPage.SendEmail.repository;
import LandingPage.SendEmail.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    User save(User user);

    Optional<User> findById(Long userId);

    @Query(value = "select u from User u where u.userName = :userName")
    User findByUserName(String userName);

    void deleteById(Long userId);

    List<User> findAll();
}