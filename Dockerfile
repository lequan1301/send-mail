FROM eclipse-temurin:17-jdk-alpine

VOLUME /tmp

ADD /target/SendEmail-0.0.1-SNAPSHOT.jar app.jar
ADD /target/dependency libs
COPY . .

EXPOSE 8080

ENTRYPOINT ["java","-jar","app.jar"]

